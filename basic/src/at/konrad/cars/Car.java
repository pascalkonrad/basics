package at.konrad.cars;

public class Car {
	public int maxv;
	public String farbe;
	public int baspreis;
	public Producer hersteller;
	public double verbrauch;


	private Engine motor;

	public Car(int maxv, String farbe, int baspreis, Producer hersteller, double verbrauch, Engine motor) {
		this.maxv = maxv;
		this.farbe = farbe;
		this.baspreis = baspreis;
		this.hersteller = hersteller;
		this.verbrauch = verbrauch;
		this.motor = motor;
	
	}

	public int getMaxv() {
		return maxv;
	}

	public void setMaxv(int maxv) {
		this.maxv = maxv;
	}

	public String getFarbe() {
		return farbe;
	}

	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}

	public int getBaspreis() {
		return baspreis;
	}

	public void setBaspreis(int baspreis) {
		this.baspreis = baspreis;
	}

	public Producer getHersteller() {
		return hersteller;
	}

	public void setHersteller(Producer hersteller) {
		this.hersteller = hersteller;
	}

	public double getVerbrauch() {
		return verbrauch;
	}

	public void setVerbrauch(double verbrauch) {
		this.verbrauch = verbrauch;
	}

	public int getPreis() {
		int fprice = this.baspreis - this.baspreis /100 * hersteller.getRabatt();
		return fprice;
	}


	public Engine getMotor() {
		return motor;
	}

	public void setMotor(Engine motor) {
		this.motor = motor;
	}

	

}
