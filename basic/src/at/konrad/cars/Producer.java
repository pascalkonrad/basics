package at.konrad.cars;

public class Producer  {
	public String herkunftsland ;
	public String name;
	public int rabatt;
	
	public Producer (String herkunftsland, String name, int rabatt) {

		this.herkunftsland = herkunftsland;
		this.name = name;
		this.rabatt = rabatt;
		
	}
	public int skontorechner() {
		return this.rabatt;
	}
	public String getHerkunftsland() {
		return herkunftsland;
	}
	public void setHerkunftsland(String herkunftsland) {
		this.herkunftsland = herkunftsland;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getRabatt() {
		return rabatt;
	}
	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}
	
	

}
