package at.konrad.basics;

import at.konrad.basics.Device.type;

public class RemoteStarter {

	public static void main(String[] args) {
		
		Remote r1 = new Remote(70,20,10,"123A", );
		Remote r2 = new Remote(50,5,10,"123B");
		
		Device d1 = new Device(1234, type.light);
		
		r1.addDevices(d1);
		
		r2.sayValues();
		System.out.println(d1.getDeviceType());

	}

}
