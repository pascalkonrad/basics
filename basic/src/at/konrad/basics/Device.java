package at.konrad.basics;

public class Device {

	private int serialNumber;
	private type deviceType;

	public enum type {

		beamer, light, television

	}

	public Device(int serialNumber, type deviceType) {
		super();
		this.serialNumber = serialNumber;
		this.deviceType = deviceType;
	}

	public int getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}

	public type getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(type deviceType) {
		this.deviceType = deviceType;
	}

}
